/**
 * @format
 * @flow strict-local
 */

import React, {useContext, useState} from 'react';
import type {Node} from 'react';
import {
  StyleSheet,
  Image,
  Pressable,
  Text,
  View,
  ImageBackground,
} from 'react-native';
import AppContext from '../components/Context';
import NextButton from '../components/NextButton';
import Carousel from '../components/Carousel';

const Deposit: () => Node = () => {
  const context = useContext(AppContext);
  const isPortrait = context.portrait;
  const [checked, setChecked] = useState(false);

  return (
    <View
      style={[
        styles.container,
        isPortrait ? styles.containerPoirtret : styles.containerLandscape,
      ]}>
      <View style={[styles.amountBlock, {width: isPortrait ? '100%' : '50%'}]}>
        <Text style={styles.header}>
          {'Choose deposit method'.toUpperCase()}
        </Text>
        <Carousel />
      </View>
      <View style={[styles.formBlock, {width: isPortrait ? '100%' : '50%'}]}>
        <View
          style={[
            styles.agreement,
            {
              justifyContent: isPortrait ? 'flex-start' : 'flex-end',
            },
          ]}>
          <Pressable
            style={styles.iconWrapper}
            onPress={() => setChecked(!checked)}>
            {checked && (
              <Image
                source={require('../assets/checkmark.png')}
                style={styles.icon}
              />
            )}
          </Pressable>
          <Text style={styles.agreementText}>
            I am older than 18 and I have read Privacy
          </Text>
        </View>
        <NextButton checked={checked} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '80%',
    width: '100%',
  },
  containerPoirtret: {
    flexDirection: 'column',
    padding: 10,
  },
  containerLandscape: {
    flexDirection: 'row',
    padding: 16,
  },
  header: {
    color: 'white',
    textTransform: 'uppercase',
    fontWeight: '800',
    fontSize: 18,
    letterSpacing: 1,
    textShadowColor: '#916FDF',
    textShadowRadius: -10,
    textDecorationStyle: 'solid',
  },
  amountBlock: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  formBlock: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconWrapper: {
    backgroundColor: '#12052D',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#4620A4',
    shadowColor: '#4620A4',
    shadowRadius: 10,
    borderRadius: 2,
    height: 20,
    width: 20,
    marginRight: 10,
  },
  buttonWrapper: {
    borderRadius: 2,
    width: '100%',
  },
  button: {
    paddingVertical: 10,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'gray',
  },
  buttonText: {
    color: 'white',
    fontWeight: '800',
    fontSize: 20,
  },
  icon: {
    height: 20,
    width: 20,
    justifyContent: 'center',
  },
  agreement: {
    marginVertical: 10,
    width: '100%',
    flexDirection: 'row',
  },
  agreementText: {
    color: 'white',
    maxHeight: 164,
    fontWeight: 'bold',
  },
});

export default Deposit;
