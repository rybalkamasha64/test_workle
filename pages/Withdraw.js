/**
 * @format
 * @flow strict-local
 */

import React, {useContext} from 'react';
import type {Node} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import AppContext from '../components/Context';
import NextButton from '../components/NextButton';

const Withdraw: () => Node = () => {
  const context = useContext(AppContext);
  const isPortrait = context.portrait;

  const agreement =
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

  return (
    <View
      style={[
        styles.container,
        isPortrait ? styles.containerPoirtret : styles.containerLandscape,
      ]}>
      <View style={[styles.amountBlock, {width: isPortrait ? '100%' : '50%'}]}>
        <Text style={styles.header}>{'Enter the amount'.toUpperCase()}</Text>
        {!isPortrait && (
          <Text style={[styles.agreement, {maxWidth: 170}]}>*{agreement}</Text>
        )}
      </View>
      <View style={[styles.formBlock, {width: isPortrait ? '100%' : '50%'}]}>

        <NextButton checked />
      </View>
      {isPortrait && <Text style={styles.agreement}>*{agreement}</Text>}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '80%',
    width: '100%',
  },
  containerPoirtret: {
    flexDirection: 'column',
    padding: 10,
  },
  containerLandscape: {
    flexDirection: 'row',
    padding: 16,
  },
  header: {
    color: 'white',
    textTransform: 'uppercase',
    fontWeight: '800',
    fontSize: 18,
    letterSpacing: 1,
    textShadowColor: '#916FDF',
    textShadowRadius: -10,
    textDecorationStyle: 'solid',
  },
  amountBlock: {
    flexDirection: 'column',
  },
  formBlock: {
    flexDirection: 'column',
  },
  agreement: {
    color: '#B190FF',
    maxHeight: 164,
    marginTop: 30,
  },
});

export default Withdraw;
