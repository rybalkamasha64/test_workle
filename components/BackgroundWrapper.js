/**
 * @format
 * @flow strict-local
 */

import React from 'react';
import {StyleSheet, ImageBackground, View} from 'react-native';

const BackgroundWrapper = ({children}) => {
  return (
    <View style={styles.container}>
      <View style={{height: '85%', width: '100%'}}>
        <ImageBackground
          source={require('../assets/menu-bg.jpg')}
          resizeMode="stretch"
          style={styles.backgroundImage}>
          {children}
        </ImageBackground>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 10,
    zIndex: 0,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  backgroundImage: {
    width: '100%',
    height: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

export default BackgroundWrapper;
