/**
 * @format
 * @flow strict-local
 */

import React from 'react';
import Deposit from '../pages/Deposit';
import Withdraw from '../pages/Withdraw';

const pages = [<></>, <></>, <></>, <Deposit />, <Withdraw />, <></>];

const MainContent = ({pageIndex}) => {
  return <>{pages[pageIndex]}</>;
};

export default MainContent;
