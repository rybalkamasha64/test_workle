/**
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Text,
  ScrollView,
  ImageBackground,
} from 'react-native';
import {act} from 'react-test-renderer';

const arrow = require('../assets/arrow-left.png');

const Carousel = () => {
  const cards = [
    {
      id: 0,
      name: 'MasterCard',
      icon: require('../assets/master-card.png'),
    },
    {
      id: 1,
      name: 'Visa',
      icon: require('../assets/visa.png'),
    },
    {
      id: 2,
      name: 'MasterCard',
      icon: require('../assets/master-card.png'),
    },
    {
      id: 3,
      name: 'Visa',
      icon: require('../assets/visa.png'),
    },
    {
      id: 4,
      name: 'MasterCard',
      icon: require('../assets/master-card.png'),
    },
    {
      id: 5,
      name: 'Visa',
      icon: require('../assets/visa.png'),
    },
  ];

  const [activeCard, setActiveCard] = useState(0);

  function onScrollCar(event){
  }
  const ref = React.useRef();

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.arrowWrapper}
        disabled={activeCard === 0}
        onPress={() => {
          setActiveCard(activeCard - 1);
          ref.current.scrollTo((activeCard - 1) * 40);
        }}>
        <Image source={arrow} style={styles.arrowUp} />
      </TouchableOpacity>
      <ScrollView ref={ref} style={styles.menu} showsVerticalScrollIndicator={false} onScroll={(event)=>{onScrollCar(event)}}>
        <TouchableOpacity style={{height: 60}}></TouchableOpacity>
        {cards.map(card => {
          return (
            <View
              key={card.id}
              style={styles.menuItem}>
              <ImageBackground
                source={require('../assets/filter-btn-bg.png')}
                style={styles.buttonBack}
                resizeMode="stretch">
                {(activeCard === card.id) && <Image source={card.icon} style={styles.icon} />}
                <Text style={styles.menuItemText}>{card.name}</Text>
              </ImageBackground>
            </View>
          );
        })}
        <TouchableOpacity style={{height: 60}}></TouchableOpacity>
      </ScrollView>
      <TouchableOpacity
        style={styles.arrowWrapper}
        disabled={activeCard === cards.length - 1}
        onPress={() => {
          setActiveCard(activeCard + 1);
          ref.current.scrollTo((activeCard + 1) * 40);
        }}>
        <Image source={arrow} style={styles.arrowDown} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    maxHeight: '65%',
    overflow: 'scroll',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    padding: 0,
  },
  menu: {
    height: '100%',
    flexDirection: 'column',
  },
  menuItem: {
    height: 37,
    width: '100%',
    shadowColor: '#00004B',
    shadowOffset: {width: 0, height: 5},
    shadowRadius: 10,
    shadowOpacity: 0.35,
  },
  buttonBack: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    flexDirection: 'row',
  },
  menuItemText: {
    color: 'white',
    fontWeight: 'bold',
    textShadowColor: '#1C0656',
    textShadowOffset: {width: 0, height: 5},
    textShadowRadius: 10,
  },
  arrowWrapper: {
    width: 'auto',
    padding: 5,
    margin: 0,
  },
  arrowUp: {
    width: 20,
    height: 20,
    transform: [{rotate: '90deg'}],
  },
  arrowDown: {
    width: 20,
    height: 20,
    transform: [{rotate: '270deg'}],
  },
  icon: {
    width: 20,
    height: 20,
    margin: 4,
  },
});

export default Carousel;
