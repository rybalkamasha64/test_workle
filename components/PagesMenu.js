/**
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Text,
  ScrollView,
  ImageBackground,
  DeviceEventEmitter,
} from 'react-native';

const arrow = require('../assets/arrow-left.png');

const PagesMenu = ({active}) => {
  const pages = [
    {
      id: 0,
      name: 'Balance',
    },
    {
      id: 1,
      name: 'Change password',
    },
    {
      id: 2,
      name: 'Game history',
    },
    {
      id: 3,
      name: 'Deposit',
    },
    {
      id: 4,
      name: 'Withdraw',
    },
    {
      id: 5,
      name: 'QR Code',
    },
  ];

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.arrowWrapper}
        disabled={active === 0}
        onPress={() => {
          DeviceEventEmitter.emit('prevPage');
        }}>
        <Image source={arrow} style={styles.arrowLeft} />
      </TouchableOpacity>
      <ScrollView
        style={styles.menu}
        horizontal
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}>
        {pages.map((pageData) => {
          return pageData.id === active ? (
            <TouchableOpacity
              key={pageData.id}
              style={styles.menuItem}
              onPress={() => {
                DeviceEventEmitter.emit('pageClick', pageData.id);
              }}>
              <ImageBackground
                source={require('../assets/filter-btn-selected.png')}
                resizeMode="stretch"
                style={[styles.buttonBack, {maxHeight: 59}]}>
                <Text style={styles.menuItemText}>{pageData.name}</Text>
              </ImageBackground>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              key={pageData.id}
              style={styles.menuItem}
              onPress={() => {
                DeviceEventEmitter.emit('pageClick', pageData.id);
              }}>
              <ImageBackground
                source={require('../assets/filter-btn-bg.png')}
                style={styles.buttonBack}
                resizeMode="stretch">
                <Text style={styles.menuItemText}>{pageData.name}</Text>
              </ImageBackground>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
      <TouchableOpacity
        style={styles.arrowWrapper}
        disabled={active === pages.length - 1}
        onPress={() => {
          DeviceEventEmitter.emit('nextPage');
        }}>
        <Image source={arrow} style={styles.arrowRight} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    padding: 0,
  },
  menu: {
    padding: 0,
    margin: 0,
    width: '100%',
    flexDirection: 'row',
    overflowX: 'hidden',
  },
  menuItem: {
    zIndex: 20,
    padding: 0,
    margin: 0,
    height: 37,
    shadowColor: '#00004B',
    shadowOffset: {width: 0, height: 5},
    shadowRadius: 10,
    shadowOpacity: 0.35,
  },
  buttonBack: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 25,
    height: '100%',
  },
  menuItemText: {
    color: 'white',
    fontWeight: 'bold',
    textShadowColor: '#1C0656',
    textShadowOffset: {width: 0, height: 5},
    textShadowRadius: 10,
  },
  arrowWrapper: {
    height: '100%',
    width: 'auto',
    backgroundColor: '#1B0743',
    borderRadius: 2,
    padding: 5,
    margin: 0,
  },
  arrowLeft: {
    width: 29,
    height: 29,
  },
  arrowRight: {
    width: 29,
    height: 29,
    transform: [{scaleX: -1}],
  },
});

export default PagesMenu;
