/**
 * @format
 * @flow strict-local
 */
import React, {useContext, useEffect, useState} from 'react';
import {ImageBackground, StyleSheet, View} from 'react-native';
import AppContext from './Context';

const Header = () => {
  const context = useContext(AppContext);
  const isPortrait = context.portrait;
  const [headerStyle, setHeaderStyle] = useState(
    styles.backgroundImagePortrait,
  );
  const [image, setImage] = useState(
    require('../assets/search/bg-header-portrait.png'),
  );

  useEffect(() => {
    isPortrait
      ? setHeaderStyle(styles.backgroundImagePortrait)
      : setHeaderStyle(styles.backgroundImageLandscape);

    isPortrait
      ? setImage(require('../assets/search/bg-header-portrait.png'))
      : setImage(require('../assets/search/bg-header-landscape.png'));
  }, [isPortrait]);

  return (
    <View style={styles.container}>
      <ImageBackground
        source={image}
        resizeMode="stretch"
        style={headerStyle}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    zIndex: 10,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  backgroundImageLandscape: {
    top: 10,
    width: '100%',
    height: 47,
    justifyContent: 'center',
  },
  backgroundImagePortrait: {
    top: 25,
    left: -5,
    width: '105%',
    height: 87,
    justifyContent: 'center',
  },
});

export default Header;
