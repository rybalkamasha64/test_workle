/**
 * @format
 * @flow strict-local
 */

import React, {useContext, useEffect, useState} from 'react';
import {Image, Linking, Pressable, StyleSheet, Text, View} from 'react-native';
import AppContext from './Context';

function openLink(link) {
  Linking.canOpenURL(link).then(supported => {
    if (supported) {
      Linking.openURL(link);
    } else {
      alert('Something wrong');
    }
  });
}

const langs = {
  Eng: require('../assets/lang/en.png'),
  Rus: require('../assets/lang/ru.png'),
  Zh: require('../assets/lang/zh.png'),
};

const Contacts = () => {
  const contacts = [
    {
      id: 0,
      link: 'https://twitter.com/habr_com/status/1557717224365899776/photo/1',
    },
    {
      id: 1,
      link: 'https://twitter.com/habr_com/status/1557717224365899776/photo/1',
    },
    {
      id: 2,
      link: 'https://twitter.com/habr_com/status/1557717224365899776/photo/1',
    },
    {
      id: 3,
      link: 'https://twitter.com/habr_com/status/1557717224365899776/photo/1',
    },
  ];
  const context = useContext(AppContext);
  const isPortrait = context.portrait;
  const language = context.lang;
  const sound = context.sound ? 'on' : 'off';
  const soundIcon = context.sound
    ? require('../assets/sound-on.png')
    : require('../assets/sound-off.png');

  return (
    <View style={styles.container}>
      <View style={styles.iconsWrapper}>
        <Text style={styles.text}>Technical Support</Text>
        {!isPortrait ? (
          <View style={{marginLeft: 8, flexDirection: 'row'}}>
            {contacts.map(contact => (
              <Pressable
                key={contact.id}
                style={[styles.iconWrapper, styles.social]}
                onPress={() => openLink(contact.link)}>
                <Image
                  source={require('../assets/twitter.png')}
                  style={styles.icon}
                />
              </Pressable>
            ))}
          </View>
        ) : (
          <Pressable
            style={[styles.iconWrapper, styles.social, {marginLeft: 8}]}
            onPress={() => openLink(contacts[0].link)}>
            <Image
              source={require('../assets/twitter.png')}
              style={styles.icon}
            />
          </Pressable>
        )}
      </View>
      <View style={styles.settingsWrapper}>
        {!isPortrait && <Text style={styles.text}>{language}</Text>}
        <Pressable style={[styles.iconWrapper, styles.lang]}>
          <Image source={langs[language]} style={styles.icon} />
        </Pressable>

        <Pressable
          style={[styles.iconWrapper, styles.lang, {marginLeft: 8}]}
          onPress={() => context.setSound(!context.sound)}>
          <Image source={soundIcon} style={styles.icon} />
        </Pressable>
        {!isPortrait && (
          <Text style={[styles.text, {marginLeft: 4}]}>Sound {sound}</Text>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    color: '#9C7FE2',
    fontWeight: 'bold',
  },
  container: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#300F85',
  },
  icon: {
    height: 20,
    width: 20,
    justifyContent: 'center',
  },
  iconsWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconWrapper: {
    backgroundColor: '#271063',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#4620A4',
    borderRadius: 2,
    marginLeft: 4,
  },
  settingsWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  social: {
    padding: 4,
    marginLeft: 4,
  },
  lang: {
    padding: 2,
  },
});

export default Contacts;
