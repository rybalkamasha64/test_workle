import React from 'react';
import type {Node} from 'react';
import {StyleSheet} from 'react-native';

const BottomButton: (text: String) => Node = text => {
  return (
    <>
      <button>{text}</button>
    </>
  );
};

const styles = StyleSheet.create({});

export default BottomButton;
