/**
 * @format
 * @flow strict-local
 */

import React, {useContext, useEffect, useState} from 'react';
import {ImageBackground, StyleSheet, View} from 'react-native';
import AppContext from './Context';

const Footer = () => {
  const context = useContext(AppContext);
  const isPortrait = context.portrait;
  const [footerStyle, setFooterStyle] = useState(
    styles.backgroundImageSecPortrait,
  );
  const [image, setImage] = useState(
    require('../assets/footer/portrait-secondary-bg.png'),
  );

  useEffect(() => {
    isPortrait
      ? setFooterStyle(styles.backgroundImageSecPortrait)
      : setFooterStyle(styles.backgroundImageSecLandscape);

    isPortrait
      ? setImage(require('../assets/footer/portrait-secondary-bg.png'))
      : setImage(require('../assets/footer/landscape/bg.png'));
  }, [isPortrait]);

  return (
    <View style={styles.container}>
      <ImageBackground
        source={image}
        resizeMode="stretch"
        style={footerStyle}
      />
      {isPortrait && (
        <ImageBackground
          source={require('../assets/footer/portrait-main-bg.png')}
          resizeMode="stretch"
          style={styles.backgroundImageMain}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 2,
    zIndex: 10,
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  backgroundImageMain: {
    bottom: -30,
    width: '100%',
    height: '78%',
    justifyContent: 'center',
  },
  backgroundImageSecPortrait: {
    position: 'absolute',
    bottom: -4,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
  },
  backgroundImageSecLandscape: {
    width: '100%',
    height: '120%',
    justifyContent: 'center',
  },
});

export default Footer;
