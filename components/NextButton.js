/**
 * @format
 * @flow strict-local
 */

import React from 'react';
import {StyleSheet, Pressable, Text, ImageBackground} from 'react-native';

const NextButton = ({checked, effect = () => {}}) => {
  return (
    <Pressable
      style={styles.buttonWrapper}
      onPress={checked ? effect : () => {}}>
      {checked ? (
        <ImageBackground
          source={require('../assets/btn-green.png')}
          style={styles.button}
          resizeMode="stretch">
          <Text style={styles.buttonText}>NEXT</Text>
        </ImageBackground>
      ) : (
        <ImageBackground
          source={require('../assets/btn-gray.png')}
          style={styles.button}
          resizeMode="stretch">
          <Text style={[styles.buttonText, {color: '#BAB7BB'}]}>NEXT</Text>
        </ImageBackground>
      )}
    </Pressable>
  );
};

const styles = StyleSheet.create({
  buttonWrapper: {
    borderRadius: 2,
    width: '100%',
  },
  button: {
    paddingVertical: 10,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'gray',
  },
  buttonText: {
    color: 'white',
    fontWeight: '800',
    fontSize: 18,
  },
});

export default NextButton;
