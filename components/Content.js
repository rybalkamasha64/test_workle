/**
 * @format
 * @flow strict-local
 */

import React from 'react';
import BackgroundWrapper from '../components/BackgroundWrapper';
import PagesMenu from './PagesMenu';
import MainContent from './MainContent';
import Contacts from './Contacts';
import {StyleSheet} from 'react-native';

const Content = ({currentPage}) => {
  return (
    <BackgroundWrapper>
      <PagesMenu active={currentPage} />
      <MainContent style={styles.container} pageIndex={currentPage} />
      <Contacts />
    </BackgroundWrapper>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 30,
  },
});

export default Content;
