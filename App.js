/**
 * @format
 * @flow strict-local
 */
import AppContext from './components/Context';

import React, {useEffect, useState} from 'react';
import type {Node} from 'react';
import {DeviceEventEmitter, Dimensions, StyleSheet, View} from 'react-native';

import Footer from './components/Footer';
import Header from './components/Header';
import Content from './components/Content';

const App: () => Node = () => {
  const [curPage, setCurPage] = useState(3);
  DeviceEventEmitter.addListener('pageClick', event => {
    setCurPage(event);
  });
  DeviceEventEmitter.addListener('nextPage', event => {
    setCurPage(curPage + 1);
  });
  DeviceEventEmitter.addListener('prevPage', event => {
    setCurPage(curPage - 1);
  });

  const [isPortrait, setIsPortrait] = useState(true);
  const [lang, setLang] = useState('Eng');
  const [sound, setSound] = useState(true);

  const stateObject = {
    portrait: isPortrait,
    lang: lang,
    sound: sound,
    setIsPortrait,
    setLang,
    setSound,
  };

  useEffect(() => {
    const subscription = Dimensions.addEventListener('change', () => {
      const screen = Dimensions.get('screen');
      setIsPortrait(screen.height >= screen.width);
    });

    return () => subscription?.remove();
  });

  return (
    <AppContext.Provider value={stateObject}>
      <View style={styles.container}>
        <Header />
        <Content currentPage={curPage} />
        <Footer />
      </View>
    </AppContext.Provider>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 0,
    flex: 1,
    padding: 0,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
  },
});

export default App;
